//深入浅出说CUDA程序设计（三）详细 http://blog.csdn.net/shi06/article/details/6625534



/*
http://datamining.xmu.edu.cn/bbs/forum.php?mod=viewthread&tid=475
CUDA ，MPI，Hadoop都是并行运算的工具。
CUDA是基于GPU芯片计算。
简单这么理解。GPU有很多个核(几百个)，每个核可以跑一个线程，多个线程组成一个单位叫做块。

我们可以举个例子：
有三个向量 int a[10], b[10], c[10];
我们要计算a和b的向量之和存放到c中。
一般C语言：
for(int i=0; i<10; i++)
  c[i] = a[i] + b[i];
复制代码
CUDA编程做法：
GPU中的每个线程(核)有一个独立序号叫index，那么只要序号从0到9的线程执行c[index] = a[index] + b[index];就可以实现以上的for循环。
GPU的可贵之处就是，可以并发运行多个线程，相当于一个时间内赋值10次。
*/

////////////////////////
cuda.cu
////////////////////////
#include <stdio.h>
#include <cuda_runtime.h>

/* 运行在GPU端的程序 */
__global__ void vectorADD(int* a, int* b, int* c)
{
     int index = threadIdx.x;//获得当前线程的序号
     if(index < blockDim.x)
         c[index] = a[index] + b[index];
}

int main ()
{
        /* 定义10个GPU运算线程 */
        int N = 10;
        
        /* 本地开辟三个数组存放我们要计算的内容 */
        int* h_a = (int*) malloc (N * sizeof(int));
        int* h_b = (int*) malloc (N * sizeof(int));
        int* h_c = (int*) malloc (N * sizeof(int));
        /* 初始化数组A, B和C */
        for(int i=0; i<N; i++)
        {
                h_a[i] = i;
                h_b[i] = i;
                h_c[i] = 0;
        }

        /* 计算10个int型需要的空间 */
        int size = N * sizeof(int);
        
        /* 在GPU上分配同样大小的三个数组 */
        int* d_a;
        int* d_b;
        int* d_c;
        cudaMalloc((void**)&d_a, size);
        cudaMalloc((void**)&d_b, size);
        cudaMalloc((void**)&d_c, size);

        /* 把本地的数组拷贝进GPU内存 */
        cudaMemcpy(d_a, h_a, size, cudaMemcpyHostToDevice);
        cudaMemcpy(d_b, h_b, size, cudaMemcpyHostToDevice);
        cudaMemcpy(d_c, h_c, size, cudaMemcpyHostToDevice);
        
        /* 定义一个GPU运算块 由 10个运算线程组成 */
        dim3 DimBlock = N;
        /* 通知GPU用10个线程执行函数vectorADD */
        vectorADD<<<1, DimBlock>>>(d_a, d_b, d_c);
        /* 将GPU运算完的结果复制回本地 */
        cudaMemcpy(h_c, d_c, size, cudaMemcpyDeviceToHost);
        
        /* 释放GPU的内存 */
        cudaFree(d_a);
        cudaFree(d_b);
        cudaFree(d_c);

        /* 验证计算结果 */
        for(int j=0; j<N; j++)
                printf("%d ", h_c[j]);
        printf("\n");
}
//cuda程序以cu为后缀，使用nvcc编译器。用法和gcc有相似之处。







/*
python cuda
http://documen.tician.de/pycuda/
https://pypi.python.org/pypi/pycuda


*/